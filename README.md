Quarkus with Keycloak
=====================

## Requirements

To compile and run this demo you will need:

- JDK 1.8+
- GraalVM
- Keycloak

### Configuring GraalVM and JDK 1.8+

Make sure that both the `GRAALVM_HOME` and `JAVA_HOME` environment variables have
been set, and that a JDK 1.8+ `java` command is on the path.

See the [Building a Native Executable guide](https://quarkus.io/guides/building-native-image)
for help setting up your environment.


# Start
- Clone this repo
- Start the Keycloak server
```
docker run --rm --name keycloak \
-e KEYCLOAK_USER=admin \
-e KEYCLOAK_PASSWORD=admin \
-e KEYCLOAK_IMPORT=/opt/app-root/quarkus-realm.json \
-p 8180:8080 \
registry.gitlab.com/conclusionxforce/container/quarkus-keycloak
```
This command will pull a pre-build keycloak server with the Quarkus realm already imported.

You should be able to access your Keycloak Server at http://localhost:8180/auth[localhost:8180/auth].

Log in as the `admin` user to access the Keycloak Administration Console.
Username should be `admin` and password `admin`.

- Live coding with Quarkus
```
cd security-keycloak-authorization-quickstart
./mvnw quarkus:dev
```
This command will leave Quarkus running in the foreground listening on port 8080.

# Testing the Application

The application is using bearer token authorization and the first thing to do is obtain an access token from the Keycloak Server in order to access the application resources:
```
export access_token=$(\
    curl -s -X POST http://localhost:8180/auth/realms/quarkus/protocol/openid-connect/token \
    --user backend-service:secret \
    -H 'content-type: application/x-www-form-urlencoded' \
    -d 'username=alice&password=alice&grant_type=password' | jq --raw-output '.access_token' \
 ) | echo $access_token
```
The example above obtains an access token for user alice.

Any user is allowed to access the http://localhost:8080/api/users/me endpoint which basically returns a JSON payload with details about the user.
```
curl -v -X GET \
  http://localhost:8080/api/users/me \
  -H "Authorization: Bearer "$access_token
```
The http://localhost:8080/api/admin endpoint can only be accessed by users with the admin role. If you try to access this endpoint with the previously issued access token, you should get a 403 response from the server.
```
 curl -v -X GET \
   http://localhost:8080/api/admin \
   -H "Authorization: Bearer "$access_token
```
In order to access the admin endpoint you should obtain a token for the admin user:
```
export access_token=$(\
    curl -s -X POST http://localhost:8180/auth/realms/quarkus/protocol/openid-connect/token \
    --user backend-service:secret \
    -H 'content-type: application/x-www-form-urlencoded' \
    -d 'username=admin&password=admin&grant_type=password' | jq --raw-output '.access_token' \
 ) | echo $access_token
```

# Run Quarkus as a native executable

You can also create a native executable from this application without making any
source code changes. A native executable removes the dependency on the JVM:
everything needed to run the application on the target platform is included in 
the executable, allowing the application to run with minimal resource overhead.

Compiling a native executable takes a bit longer, as GraalVM performs additional
steps to remove unnecessary codepaths. Use the  `native` profile to compile a
native executable:

> ./mvnw package -Dnative

After getting a cup of coffee, you'll be able to run this executable directly:

> ./target/security-keycloak-authorization-quickstart-1.0-SNAPSHOT-runner